import React from "react";
import "./styles.css";

const PaymentInfo = () => {
  return (
    <div className="dashobard-body-content">
      <h5>NEXT DIRECT DEBIT</h5>
      <h2>$356.95 in 10 days (29 Jul)</h2>
      <div className="horizontal-line" />
      <h5>PAYMENT FREQUENCY</h5>
      <h2>Weekly on Wednesdays</h2>
      <div className="horizontal-line" />
    </div>
  );
};

export default PaymentInfo;
