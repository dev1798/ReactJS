import React from "react";
import RightSubMenu from "./RightSubMenu";
import "./RightMenu.css";

const RightMenu = () => {
  return (
    <div className="menu-container">
      <RightSubMenu title="Make an extra payment" imgClass="dollar" />
      <div className="horizontal-line" />
      <RightSubMenu title="Change repayment date" imgClass="calendar" />
      <div className="horizontal-line" />
      <RightSubMenu title="Change repayment amount" imgClass="dollar" />
    </div>
  );
};

export default RightMenu;
