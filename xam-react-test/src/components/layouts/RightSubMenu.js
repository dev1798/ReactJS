import React from "react";
import "./RightSubMenu.css";

const RightSubMenu = ({ title, imgClass }) => {
  return (
    <div className="right-submenu-container">
      <div className={`right-submenu-content img-${imgClass}`}>{title}</div>
    </div>
  );
};

export default RightSubMenu;
