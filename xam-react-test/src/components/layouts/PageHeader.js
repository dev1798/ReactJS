import React from "react";
import "./PageHeader.css";

const PageHeader = ({ title }) => {
  return (
    <div className="header-color">
      <h1 className="blue-font">{title}</h1>
      <div className="horizontal-line-header" />
    </div>
  );
};

export default PageHeader;
