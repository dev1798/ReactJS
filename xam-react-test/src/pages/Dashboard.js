import React from "react";
import PageHeader from "../components/layouts/PageHeader";
import RightMenu from "../components/layouts/RightMenu";
import PaymentInfo from "../components/dashboard";
import "./Dashboard.css";

const Dashboard = () => {
  return (
    <div className="dashboard-content">
      <PageHeader title="Repayments" />
      <div className="dashboard-body-content">
        <div className="col70">
          <PaymentInfo />
        </div>
        <div className="col30">
          <RightMenu />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
