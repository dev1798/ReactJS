import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./fonts/Garnett-Light.ttf";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
