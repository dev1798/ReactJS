import React from "react";

class SearchBar extends React.Component {
  state = { searchStr: "" };
  onSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state.searchStr);
  };
  render() {
    return (
      <div>
        <form className="ui segment" onSubmit={this.onSubmit}>
          <div className="ui form">
            <div className="ui icon input field" style={{ width: "100%" }}>
              <input
                className="prompt"
                type="text"
                onChange={(e) => {
                  this.setState({ searchStr: e.target.value });
                }}
                value={this.state.searchStr}
                placeholder="search here"
              />
              <i className="search icon" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
