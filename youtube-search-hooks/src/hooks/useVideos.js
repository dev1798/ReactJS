import { useState, useEffect } from "react";
import YouTube from "../api/YouTube";

const useVideos = (defaultSearchTerm) => {
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    search(defaultSearchTerm);
  }, [defaultSearchTerm]);

  const search = async (searchStr) => {
    const response = await YouTube.get("/search", {
      params: {
        q: searchStr,
      },
    });
    setVideos(response.data.items);
  };

  return [videos, search];
};

export default useVideos;
