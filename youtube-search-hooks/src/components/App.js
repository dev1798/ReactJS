import { useState, useEffect } from "react";
import SearchBar from "./SearchBar";
import VideoDetails from "./VideoDetails";
import VideoList from "./VideoList";
import YouTube from "../api/YouTube";
import useVideos from "../hooks/useVideos";

const App = () => {
  const [videos, search] = useVideos("New Telugu Videos");
  const [selectedVideo, setSelectedVideo] = useState(null);

  useEffect(() => {
    setSelectedVideo(videos[0]);
  });

  return (
    <div className="ui container">
      <SearchBar onSubmit={search}></SearchBar>
      <div className="ui segment grid">
        <div className="ui row">
          <div className="eleven wide column">
            <VideoDetails video={selectedVideo} />
          </div>
          <div className="five wide column">
            <VideoList
              videos={videos}
              onVideoSelect={(video) => setSelectedVideo(video)}
            ></VideoList>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
