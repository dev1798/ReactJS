import React, { useState } from "react";

const SearchBar = ({ onSubmit }) => {
  const [searchStr, setSearchStr] = useState("");

  const onFormSubmit = (e) => {
    e.preventDefault();
    onSubmit(searchStr);
  };

  return (
    <div>
      <form className="ui segment" onSubmit={onFormSubmit}>
        <div className="ui form">
          <div className="ui icon input field" style={{ width: "100%" }}>
            <input
              className="prompt"
              type="text"
              onChange={(e) => {
                setSearchStr(e.target.value);
              }}
              value={searchStr}
              placeholder="search here"
            />
            <i className="search icon" />
          </div>
        </div>
      </form>
    </div>
  );
};

export default SearchBar;
