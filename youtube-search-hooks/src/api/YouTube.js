import axios from "axios";

const KEY = "AIzaSyDUYiNwXeF3QlZCH6-CLFiDYiKDSGY4Hbk";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    type: "video",
    maxResults: 5,
    key: KEY,
  },
});
